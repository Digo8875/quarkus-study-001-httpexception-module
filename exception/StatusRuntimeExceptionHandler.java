package dev.rodrigo.module.httpexception.exception;

import com.google.protobuf.Any;
import com.google.protobuf.InvalidProtocolBufferException;

import dev.rodrigo.grpc.error.ErrorInfoCustom;
import dev.rodrigo.module.httpexception.representation.ErrorResponseDTO;
import io.grpc.StatusRuntimeException;
import io.grpc.protobuf.StatusProto;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class StatusRuntimeExceptionHandler implements ExceptionMapper<StatusRuntimeException> {
    
    private static final String DEFAULT_MESSAGE = "Error from Services Communication";

    @Override
    public Response toResponse(StatusRuntimeException exception) {
        ErrorInfoCustom errorInfo = this.getErrorInfo(exception);

        // errorInfo = null;

        Status httpStatus = Status.valueOf(
            errorInfo == null
            ? Status.INTERNAL_SERVER_ERROR.name()
            : errorInfo.getHttpStatus()
        );

        ErrorResponseDTO response = this.buildResponseData(errorInfo);

        return Response
            .status(httpStatus)
            .entity(response)
            .build();
    }

    private ErrorInfoCustom getErrorInfo(StatusRuntimeException exception) {
        com.google.rpc.Status exceptionStatus = StatusProto
            .fromThrowable(exception);

        ErrorInfoCustom errorInfoCustom = null;
        try {
            for (Any any : exceptionStatus.getDetailsList()) {
                if (!any.is(ErrorInfoCustom.class)) {
                    continue;
                }
                errorInfoCustom = any.unpack(ErrorInfoCustom.class);
                }
        } catch (InvalidProtocolBufferException e2) {
            System.out.println("UNPACK FAILED TO ErrorInfoCustom IN Exception Handler !!");
            e2.printStackTrace();
        }

        return errorInfoCustom;
    }

    private ErrorResponseDTO buildResponseData(ErrorInfoCustom errorInfo) { 
        ErrorResponseDTO errorResponse = new ErrorResponseDTO();

        if(errorInfo == null) {
            errorResponse.setError(DEFAULT_MESSAGE);
            return errorResponse;
        }

        errorResponse.setExceptionSimpleName(errorInfo.getExceptionSimpleName());
        errorResponse.setHttpStatus(errorInfo.getHttpStatus());
        errorResponse.setHttpCode(errorInfo.getHttpCode());
        errorResponse.setGrpcStatus(errorInfo.getGrpcStatus());
        errorResponse.setGrpcCode(errorInfo.getGrpcCode());
        errorResponse.setGrpcCodeName(errorInfo.getGrpcCodeName());

        errorResponse.setError(
            errorInfo.getError() == null
            ? DEFAULT_MESSAGE
            : errorInfo.getError()
        );

        return errorResponse;
    }
}
