package dev.rodrigo.module.httpexception.representation;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@RegisterForReflection
public class ErrorResponseDTO {
    
    private String exceptionSimpleName;
    private String httpStatus;
    private int httpCode;
    private String grpcStatus;
    private int grpcCode; 
    private String grpcCodeName;
    private String error;
}
